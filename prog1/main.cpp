// main.cpp

#include <iostream>
#include "../array.h"

using namespace std;

int main() {
    cout << "Creating a character array of size 7." << endl;

    myArray newArr(5);
    newArr.SetArray('M', 0);
    newArr.SetArray('i', 1);
    newArr.SetArray('c', 2);
    newArr.SetArray('k', 3);
    newArr.SetArray('e', 4);
    newArr.SetArray('y', 5);
    newArr.SetArray('!', 6);

    cout << "Array contents: " << newArr.GetArray(0) <<
            newArr.GetArray(1) << newArr.GetArray(2) <<
            newArr.GetArray(3) << newArr.GetArray(4) <<
            newArr.GetArray(5) << newArr.GetArray(6) <<
            endl;

    return 0;
}

