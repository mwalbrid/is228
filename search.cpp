#include <cstdlib>
#include <iostream>
#include "search.h"

using namespace std;

int search(int A[], unsigned int L, int target, bool sorted) {

    if (L == 0) // if there is no length in the array
    {
        return -1;
    } else // there is length in the array
    {
        if (sorted == true) // binary search
        {
            int first = 0; // set the lower bound of the search 
            int last = L; // set the upperbound of the search to the length
            int middle = (first + last) / 2; // set the middle to the middle of our first and last

            while ((A[middle] != target and first <= last)) // runs while the middle is not the target
            {
                if (A[middle] > target) // middle number is greater than the target
                {
                    last = middle - 1; // lower the upperbound
                }
                else // (A[middle] < target)
                {
                    first = middle + 1; // increase the lower bound
                }
                middle = (first + last) / 2; // set the middle to the new middle
            }
            if (first <= last) // once we reach target or do not reach target
            {
                return middle; // returns the reached target
            }
            else {
                return -1; // number is not in the array
            }
        } else // linear search
        {
            int i = 0; // counter for the position
            bool found = false; // if the item is found

            while ((i < L) and !found) // while we are lower then the loop and not found
            {
                if (A[i] == target) // the target is where we are at
                {
                    found = true; // found is now true since its the target
                    return i; // return the position
                } else {
                    i++; // move forward in the array
                }
            }
            return -1; // not found in the array
        }
    }
}