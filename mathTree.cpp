// mathTree.cpp
#include <iostream>
#include <sstream>
#include "mathTree.h"
#include <stdlib.h>

using namespace std;

string stringify(long double x) { // Prof's magic wand of science 
    ostringstream o;
    if (!(o << x))
        throw;
    return o.str();
}

mathTree::mathTree() {
    // Add content here
    //Construter   
    root = NULL; //sets the root to Null
}

mathTree::~mathTree() {
    // Add content here
    //Deconstructor
    //need to make a new function to handle this

    _deleteItems(root);


}

void mathTree::_deleteItems(treeNode* t) {
    if (t -> leafNode == true) {
        delete t; // if only a leaf delete it
    } else {
        _deleteItems(t -> lChild); // delete lChild
        _deleteItems(t -> rChild); // delete rChild
        delete t; // Delete op
    }
}

void mathTree::ReadExpression(string s) {
    // Add content here
    //Error Checking 
    //Call _ReadExpression 
    string whatsleft; // makes a string for whats left over
    root = new treeNode; // new root to start

    _readex(root, s);
    
    
   // whatsleft = _readex(root, s);
    //if (whatsleft.empty()) {
      //  _deleteItems(root);
        //delete the tree  need a function to call the ~mathTree()
      //  throw "Items left over and a bad input to tree";
    //}

    //no else needed

}

string mathTree::_readex(treeNode* t, string exp) {
    // Add content here

    string remaining, firstpart;
    // make sure imputs are vaild 
    //before you throw runn the deconstructer
    // string is empty throw 

    t -> lChild = NULL; // helps prevent random data
    t -> rChild = NULL;
    if (exp.empty()) { // if our root expression is empty delete it
        _deleteItems(root); //calls the delete
        throw "Empty expression string";
    }
    // t is null throw
    if (t == NULL) { // if out root is NULL delete it
        _deleteItems(root);
        throw "Tree Node is NULL";
    }

    //step 1 seprate exp into remaining and first part
    int pos = exp.find(" "); // gets the pos of the space 
    firstpart = exp.substr(0, pos); // grabs the first part up to the space
    remaining = exp.substr(pos + 1); // deals with the space and takes the rest


    if ((firstpart[0] == '+') or (firstpart[0] == '*')) { //if an operator fills it in
        t -> leafNode = false; // not a leaf
        t -> op = firstpart[0]; // at the first spot
        t -> lChild = new treeNode; // makes a lchild for the op
        remaining = _readex(t -> lChild, remaining); // the remaining is put in its string
        t -> rChild = new treeNode; // makes a rchild for the op
        remaining = _readex(t -> rChild, remaining); // the remaining is put in its string

    } else {
        t -> leafNode = true; // its a leafnode
        t -> value = atof(exp.c_str()); // makes it a char so we can handle it
        t -> lChild = NULL; // the lchild is null
        t -> rChild = NULL; // the rchild is null

    }
    return remaining;
}

double mathTree::ExpressionValue() {
    // Add content here
    //call eval on the root
    return _evaluate(root);

}

double mathTree::_evaluate(treeNode* t) {
    // Add content here
    //the eval recurssion on the left and right
    double a = 0; // makes a variable for the lchild to do math
    double b = 0; // makes a variable for the rchild to do math
    
    
    if (t -> leafNode) {
        return t -> value; // if only at leaf return the value
        
    }
    
    else {
        a = _evaluate(t -> lChild); //recursion on the lchild so we get a
        b = _evaluate(t -> rChild); //recursion on the rchild so we get b
        if (t->op == '+') {
            return a + b; //if op is + then add
        }
        else {
            return a * b; // if op is * then multiply 
        }

    }
}

string mathTree::ReturnInfix() {
    // Add content here
    // no need to error check because other functions have checked 
    // call _inorder at root  
    return _inorder(root);
}

string mathTree::_inorder(treeNode* t) {
    // Add content here 
    //  Make string here  
    string retValue; // making a string to output

    if (t -> leafNode == true ) {
        cout << t -> value << " ";  //if looking at a leaf return that value
    } else {
        if (t -> lChild != NULL) // recursion to return the left child
        {
         cout << "( ";   
        _inorder(t->lChild);
        }
        
        cout << t-> op << " "; // return the operator
        
        if( t-> rChild != NULL)// recursion to return the left child
        {
        _inorder(t -> rChild);
        cout << " )";
        }
    }
    return retValue; // allows me to return 
}
