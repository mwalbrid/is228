// Airport.cpp

#include "Airport.h"
#include <queue>
#include <cstdlib>
#include <iostream>
#include <time.h>

using namespace std;

void Airport(
        int landingTime, // Time segments needed for one plane to land

        int takeoffTime, // Time segs. needed for one plane to take off

        double arrivalProb, // Probability that a plane will arrive in
        // any given segment to the landing queue

        double takeoffProb, // Probability that a plane will arrive in
        // any given segment to the takeoff queue

        int maxTime, // Maximum number of time segments that a plane
        // can stay in the landing queue

        int simulationLength // Total number of time segs. to simulate
        ) {
    // Insert content here

    queue<int> arrivalQ; // Arrival Queue
    queue<int> takeoffQ; // Takeoff Queue
    double landing_time_left = 0; //flag for busy runway
    double takeoff_time_left = 0; // flag for busy runway
    int landings = 0; // landings counter
    int takeoffs = 0; // takeoff counter
    int crashes = 0; //crash counter
    double totalArrivalWait = 0; //total of all wait times in Queue for arrivals
    double arrivalWait = 0; // the current wait for an item in arrival Queue
    double totalTakeoffWait = 0; //total of all wait times in Queue for takeoffs
    double takeoffWait = 0; // the current wait for an item in takeoff Queue
    double avgArrivalWait = 0; // average wait for the arrival Queue
    double avgTakeoffWait = 0; // average wait for the takeoff Queue
    srand(time(NULL)); //seeds the random generator
    
    cout << "Time for a plane to land " << landingTime << endl;
    cout << "Time for a plane to takeoff " << takeoffTime << endl;
    cout << "Chances a plane arriving " << arrivalProb << endl;
    cout << "Chances a plane wants to takeoff " << takeoffProb << endl;
    cout << "Total simulation time " << simulationLength << endl;

    for (unsigned int clock = 0; clock < simulationLength; clock++) //simulation timer
    {
        if (rand() < RAND_MAX * arrivalProb) //random arrival check
        {
            arrivalQ.push(clock); //puts item in arrival Queue at that time

        }

        if (rand() < RAND_MAX * takeoffProb) //random takeoff check
        {
            takeoffQ.push(clock); // puts an item in takeoff Queue at that time
        }

        if (!arrivalQ.empty() and (arrivalWait = (clock - arrivalQ.front())) > maxTime)
        //this checks if a plane in arrival Queue is at the max time to crash
        // will take it out of the Queue (.pop()) and adds the crash counter    
        {
        arrivalQ.pop(); 
        crashes++;
        cout << "Plane has crashed at time " << clock << endl;
        }  
        
        //If (runway busy)  use continue;
        if (landing_time_left > 0) {
            landing_time_left--; //Lowers busy runway counter
            continue; //skips the rest and increments the clock
        }
        
        //If (runway busy)  use continue;   
        if (takeoff_time_left > 0) {
            takeoff_time_left--; //Lowers busy runway counter
            continue; //skips the rest and increments the clock
        }           
        
            //(runway is free  items in landingQ) 
        else if (!(landing_time_left > 0) and !arrivalQ.empty()) {
            arrivalWait = clock - arrivalQ.front(); //the wait time of the current item
            totalArrivalWait = totalArrivalWait + arrivalWait; //adds to the wait time total  
            arrivalQ.pop(); //pop the top
            landings++; //Adds one to landing counter
            landing_time_left = landingTime - 1; // how much time left
            cout << "Plane landed at " << clock << endl;
        }
        
        
            // Runway is free and empty landingQ but items in takeoffQ
        else if (!(takeoff_time_left > 0) and arrivalQ.empty() and !takeoffQ.empty()) {
            takeoffWait = clock - takeoffQ.front(); //the wait time of the current item
            totalTakeoffWait = totalTakeoffWait + takeoffWait; //adds the wait time to total
            takeoffQ.pop(); //pops the top
            takeoffs++; //Adds one to takeoff counter
            takeoff_time_left = takeoffTime - 1; //how much time is left
            cout << "Plane took off at " << clock << endl;
        }
        
            //else Runway is free both queues empty    
        else {
            continue;
        }
    }

    //average calculations
    if (landings == 0) //so we do not / by 0
    {
        avgArrivalWait = 0;
        avgTakeoffWait = totalTakeoffWait / takeoffs;
    }
    else if (takeoffs == 0) // so we do not / by 0
    {
        avgTakeoffWait = 0;
        avgArrivalWait = totalArrivalWait / landings;
    }
    else
    {
    avgArrivalWait = totalArrivalWait / landings; 
    avgTakeoffWait = totalTakeoffWait / takeoffs;
    }
    
    // outputs    
    cout << "Number of landings " << landings << endl;
    cout << "Number of takeoffs " << takeoffs << endl;
    cout << "Number of crashes " << crashes << endl;
    cout << "Average time in arrival queue " << avgArrivalWait << endl;
    cout << "Average time in takeoff queue " << avgTakeoffWait << endl;   
    cout << "Number of planes left in landing queue " << arrivalQ.size() << endl;
    cout << "Number of planes left to take off " << takeoffQ.size() << endl;
}