// DoubleStack.h
#include <stack>
#include <cctype>

#ifndef __DOUBLESTACK_H__
#define __DOUBLESTACK_H__

class DoubleStack
{
public:
  DoubleStack();         // Default constructor, the stack is empty to start
  ~DoubleStack();        // Default destructor

  void PushA(char value); // Add "value" to the top of stack A
  void PushB(char value); // Add "value" to the top of stack B

  char PopA(); // Remove and return the item on the top of stack A
  char PopB(); // Remove and return the item on the top of stack B

  char TopA(); // Return the item on the top of stack A
  char TopB(); // Return the item on the top of stack B

  unsigned int size();        // Return the number of items in the stack

private:
    // Add appropriate content here
    char array[20]; //Makes an 20 array piece 
    int openA; // makes A stack
    int openB; // makes B stack
    int length; // Length of array
    int capacity; // Stack items used
 
};


#endif
