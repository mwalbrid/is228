// LinkedList.cpp
#include <cstdlib> //allows us to use NULL
#include <iostream>
#include "LinkedList.h"

// Default constructor, the list is empty to start

LinkedList::LinkedList() {

    head = NULL; //makes the head start off by being NULL
    length = 0; // Starts the length at zero

}

// Default destructor, must delete all nodes

LinkedList::~LinkedList() {
    while (length != 0) { //runs the delete node till we are at zero length
        DeleteNode(0); //Deletes a node 
    }


}

// Add a node containing "value" to the front

void LinkedList::InsertFront(double value) {

    Node *fresh; //Allows us to use a node call fresh
    fresh = new Node; // fresh is a new node
    fresh -> data = value; //fresh has the value now
    fresh -> next = head; // fresh points to what the first node pointed to
    head = fresh; //since this is at the front its the head 
    length++; // new node add length goes up by one
}

// Add a node containing "value" to position "index"

void LinkedList::Insert(double value, unsigned int index) {

    Node *fresh, *prev;
    fresh = new Node;
    fresh -> data = value;
    fresh -> next = prev -> next;
    prev -> next = fresh;
    prev = head;

    if (index > length) //checks if index is legal 
    {
        throw "Illegal index in insert";
    }
    
    if (index == 0) //checks to see if we are at front
    {
        InsertFront(value);
        return;
    }

    else
    {
    for (int i = 0; i < index - 1; i++) 
    { // goes to the index we need
        prev = prev -> next;
    }
    
    fresh -> next = prev -> next; //allows us to get the other nodes in the 
    prev -> next = fresh; // correct path
    length++;
    }
}

// Return value at position "index"

double LinkedList::GetNode(unsigned int index) {

    Node *cur;
    cur = head; //just a cursor 

    if (index >= length)
        throw "Illegal index"; // checks to see if index is legal
    else 
    {
        for (int i = 0; i < index; i++) 
        {
            if (index == i) 
            {
                return cur -> data; // if at index 
            } 
            else 
            {
                cur = cur -> next; // if not at index keep going
            }
        }

        return cur -> data; //returns the data 
    }

}


// Return the index of the node containing "value"

unsigned int LinkedList::Search(double value) {
    Node *cur = head;
    for (int i = 0; ; i++) {
        if (cur == NULL)
            throw "Error"; //not found
        if (cur -> data == value) // find it on the first try
            return i; // return cur /if looking for the pointer
        cur = cur -> next;

    }

}


// Delete the node at position "index", return the value that was there

double LinkedList::DeleteNode(unsigned int index) {
    Node *prev, *temp;
    prev = head;
    temp = prev;
    
    if (index >= length)
        throw "Illegal index";
        
    for (int i = 0; i < index; i++) {
        prev = prev -> next; // goes to where we want
    }
    temp = (prev -> next) -> next; // save the list to the nodes
    delete prev -> next; // deletes the node
    prev -> next = temp; // continue the list on
    length--; // since one node is gone length goes down
    
    return prev -> data; // value to be set aside 
}

// This function reverses the order of all nodes so the first node is now the
// last and the last is now the first (and all between). So a list containing 
// (4, 0, 2, 11, 29) would be (29, 11, 2, 0, 4) after Reverse() is called.

void LinkedList::Reverse() 

{
    
    Node* prev = NULL; // Starts off
    Node* prev2 = head;   // starts at head
    Node* temp; // a temp holder
    
    while (prev2 != NULL) // runs until we get to NULL
    {
        temp = prev2-> next; // sets temp to point to next
        prev2 -> next = prev; // next now equals prev so the loop back
        prev = prev2; // prev goes forward
        prev2 = temp; // get the temp ready again
    }
    head = prev; // sets the head to the end

  
}
        
       
// Return the number of nodes in the list

int LinkedList::GetLength() {

    return length; // tells it the length of the list

}
