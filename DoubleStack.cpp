// DoubleStack.cpp

#include "DoubleStack.h"
#include <iostream>
#include <stack>
#include <cctype>

// Default constructor, the stack is empty to start

DoubleStack::DoubleStack() {
    // Insert your code here 

    openA = 0; //Starts A stack on left
    openB = 19; // Starts B stack on right
    length = 20; // Array length
    capacity = 0; // Array spots used
}


// Default destructor

DoubleStack::~DoubleStack() {

    // stays empty no need in static array
}

// Add "value" to the top of stack A

void DoubleStack::PushA(char value) {
    // Insert your code here 

    if (openA > length) // checks for overflow 
    {
        throw "Overflow in push A";
    }
    if (capacity == length) {
        throw "Push A runs into B"; // checks to see if we will run into B
    }
    if (isalpha(!value)) {
        throw "Not a char"; // make sure its a Char
    }

    array[openA] = value; // puts the value at A stack
    openA++; // moves over to next open A
    capacity++; // Array slot used goes up
}

// Add "value" to the top of stack B

void DoubleStack::PushB(char value) {
    // Insert your code here 

    if (openB > length) // checks for overflow 
    {
        throw "Overflow in push B";
    }
    if (capacity == length) { //checks to see if we will run into A
        throw "Push B runs into A";
    }
    if (isalpha(!value)) {
        throw "Not a char"; // make sure its a Char
    }

    array[openB] = value; // puts the value at A stack
    openB--; // moves over to next open B 
    capacity++; // Array slot used goes up
}

// Remove and return the item on the top of stack A

char DoubleStack::PopA() {
    // Insert your code here

    if (openA == 0) { //If we pop we will get an underflow A
        throw "Will cause underflow in pop A";
    }
    openA--; //Moves us back in the stack A
    capacity--; // takes away the array slot
    return array[openA]; // does top A stack so we know what the value was
    

}

// Remove and return the item on the top of stack B

char DoubleStack::PopB() {
    // Insert your code here 

    if (openB == 19) { //If we pop we will underflow B
        throw "Will cause underflow in pop B";
    }
    openB++; //Moves us back in the stack B
    capacity--; // takes away the array slot
    return array[openB]; // does top B stack so we know what the value was
}

// Return the item on the top of stack A

char DoubleStack::TopA() {
    // Insert your code here 
    if (openA <= 0) {
        throw "To small in top A"; // checks to make sure we are in limits
    }

    return array[openA - 1]; //Returns the top value of A stack


}

// Return the item on the top of stack B

char DoubleStack::TopB() {
    // Insert your code here 
    if (openB >= 19) {
        throw "Too big to top B"; // checks to make sure we are in limits
    }

    return array[openB + 1]; //Returns the top value of B stack


}

// Return the number of items in the stack

unsigned int DoubleStack::size() {
    // Insert your code here 

    return capacity; // returns the capacity of the stacks
}